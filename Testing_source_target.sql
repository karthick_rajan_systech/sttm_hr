select * from BCMPQA.CONTACT
select * from BCMPQA.Employee
select * from BCMPQA.EmployeeDepartmentHistory
select * from BCMPQA.Department

CREATE TABLE Target_DimEmployee(
	[EmployeeKey] [int] identity(1,1) primary key NOT NULL,
	[ParentEmployeeKey] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[HireDate] [nvarchar](50) NULL,
	[Birthdate] [nvarchar](50) NULL,
	[LoginID] [nvarchar](256) NULL,
	[PhoneNo] [nvarchar](50) NULL default 'N/A',
	[EmailAddress] [nvarchar](50) NULL default 'N/A',
	[MaritalStatus] [nchar](10) NULL,
	[Gender] [nchar](10) NULL,
	[DepartmentName] [nvarchar](50) NULL default 'N/A'
)
---drop table Target_DimEmployee

Insert into Target_DimEmployee select e.EmployeeID parentemployeekey,concat(c.firstname, c.lastname) Name, 
cast(format(e.hiredate, 'dd-MMM-yyyy') as varchar) HireDate, cast(format(e.birthdate, 'dd-MMM-yyyy') as varchar) BirthDate,
concat(substring(c.lastname,1,1), c.firstname) LoginID,
iif(charindex('(',c.phone)!=0,'N/A',phone) Phone,
isnull(c.emailaddress,'N/A') emailaddress,
case
when e.maritalstatus='S' then 'SINGLE'
when e.maritalstatus='M' then 'MARRIED'
end MaritalStatus,
case
when e.Gender='M' then 'Male'
when e.Gender='F' then 'Female'
end Gender,
d.name DepartmentName
from BCMPQA.Employee e join BCMPQA.CONTACT c on e.contactid=c.contactid
join BCMPQA.EmployeeDepartmentHistory ed on e.employeeid=ed.employeeid
join BCMPQA.Department d on d.departmentid=ed.departmentid 


select * from Target_DimEmployee

---------------------FACT-----------------

CREATE TABLE Traget_Fact_Sales_Quota(
	[EmployeeKey] [int] NULL default -1,
	[TimeKey] [int] NULL default -1,
	[CalendarYear] [int] NULL default 'N/A',
	[CalendarQuarter] [int] NULL default -1,
	[Quantity] [int] NULL,
	[Unit_Price] [money] NULL,
	[SalesAmountQuota] [money] NULL default 0
)


insert into Traget_Fact_Sales_Quota SELECT t.employeekey,dt.timekey,dt.calendaryear,
dt.calendarquarter,s.quantity,s.unit_price,s.salesamount from Target_DimEmployee t join [BCMPQA].[Sales_Quota] s
on s.employeeid=t.parentemployeekey
join [BCMPQA].[DimTime] dt
on s.orderdate=dt.fulldatealternatekey

select * from Traget_Fact_Sales_Quota;

-----------------------ROWCOUNT------------------
select count(*) from (
select e.EmployeeID parentemployeekey,concat(c.firstname, c.lastname) Name, 
cast(format(e.hiredate, 'dd-MMM-yyyy') as varchar) HireDate, cast(format(e.birthdate, 'dd-MMM-yyyy') as varchar) BirthDate,
concat(substring(c.lastname,1,1), c.firstname) LoginID,
iif(charindex('(',c.phone)!=0,'N/A',phone) Phone,
isnull(c.emailaddress,'N/A') emailaddress,
case
when e.maritalstatus='S' then 'SINGLE'
when e.maritalstatus='M' then 'MARRIED'
end MaritalStatus,
case
when e.Gender='M' then 'Male'
when e.Gender='F' then 'Female'
end Gender,
d.name DepartmentName
from BCMPQA.Employee e join BCMPQA.CONTACT c on e.contactid=c.contactid
join BCMPQA.EmployeeDepartmentHistory ed on e.employeeid=ed.employeeid
join BCMPQA.Department d on d.departmentid=ed.departmentid ) SRG;


SELECT COUNT(*) FROM Target_DimEmployee;

-----------------------ROWCOUNTGROUP BY-------------------------


select count(*),NAME from (
select e.EmployeeID parentemployeekey,concat(c.firstname, c.lastname) Name, 
cast(format(e.hiredate, 'dd-MMM-yyyy') as varchar) HireDate, cast(format(e.birthdate, 'dd-MMM-yyyy') as varchar) BirthDate,
concat(substring(c.lastname,1,1), c.firstname) LoginID,
iif(charindex('(',c.phone)!=0,'N/A',phone) Phone,
isnull(c.emailaddress,'N/A') emailaddress,
case
when e.maritalstatus='S' then 'SINGLE'
when e.maritalstatus='M' then 'MARRIED'
end MaritalStatus,
case
when e.Gender='M' then 'Male'
when e.Gender='F' then 'Female'
end Gender,
d.name DepartmentName
from BCMPQA.Employee e join BCMPQA.CONTACT c on e.contactid=c.contactid
join BCMPQA.EmployeeDepartmentHistory ed on e.employeeid=ed.employeeid
join BCMPQA.Department d on d.departmentid=ed.departmentid ) SRG
GROUP BY NAME

SELECT COUNT(*),NAME FROM Target_DimEmployee
GROUP BY NAME;

-----------------------------------DISTINCT VALUES----------------

select count(*) from (
select e.EmployeeID parentemployeekey,concat(c.firstname, c.lastname) Name, 
cast(format(e.hiredate, 'dd-MMM-yyyy') as varchar) HireDate, cast(format(e.birthdate, 'dd-MMM-yyyy') as varchar) BirthDate,
concat(substring(c.lastname,1,1), c.firstname) LoginID,
iif(charindex('(',c.phone)!=0,'N/A',phone) Phone,
isnull(c.emailaddress,'N/A') emailaddress,
case
when e.maritalstatus='S' then 'SINGLE'
when e.maritalstatus='M' then 'MARRIED'
end MaritalStatus,
case
when e.Gender='M' then 'Male'
when e.Gender='F' then 'Female'
end Gender,
d.name DepartmentName
from BCMPQA.Employee e join BCMPQA.CONTACT c on e.contactid=c.contactid
join BCMPQA.EmployeeDepartmentHistory ed on e.employeeid=ed.employeeid
join BCMPQA.Department d on d.departmentid=ed.departmentid ) SRG
GROUP BY parentemployeekey
HAVING COUNT(*)>1


SELECT COUNT(*) FROM Target_DimEmployee
GROUP BY parentemployeekey
HAVING COUNT(*)>1;

SELECT * FROM Target_DimEmployee

--------------------REFERENCE INTERGITY----------------------

SELECT COUNT(*) FROM Target_DimEmployee T
LEFT JOIN (select e.EmployeeID parentemployeekey,concat(c.firstname, c.lastname) Name, 
cast(format(e.hiredate, 'dd-MMM-yyyy') as varchar) HireDate, cast(format(e.birthdate, 'dd-MMM-yyyy') as varchar) BirthDate,
concat(substring(c.lastname,1,1), c.firstname) LoginID,
iif(charindex('(',c.phone)!=0,'N/A',phone) Phone,
isnull(c.emailaddress,'N/A') emailaddress,
case
when e.maritalstatus='S' then 'SINGLE'
when e.maritalstatus='M' then 'MARRIED'
end MaritalStatus,
case
when e.Gender='M' then 'Male'
when e.Gender='F' then 'Female'
end Gender,
d.name DepartmentName
from BCMPQA.Employee e join BCMPQA.CONTACT c on e.contactid=c.contactid
join BCMPQA.EmployeeDepartmentHistory ed on e.employeeid=ed.employeeid
join BCMPQA.Department d on d.departmentid=ed.departmentid) SRC 
ON T.ParentEmployeeKey=src.parentemployeekey
where src.ParentEmployeeKey is null;

-----------------------column level check--------------------

SELECT COUNT(*) FROM Target_DimEmployee T
LEFT JOIN (select e.EmployeeID parentemployeekey,concat(c.firstname, c.lastname) Name, 
cast(format(e.hiredate, 'dd-MMM-yyyy') as varchar) HireDate, cast(format(e.birthdate, 'dd-MMM-yyyy') as varchar) BirthDate,
concat(substring(c.lastname,1,1), c.firstname) LoginID,
iif(charindex('(',c.phone)!=0,'N/A',phone) Phone,
isnull(c.emailaddress,'N/A') emailaddress,
case
when e.maritalstatus='S' then 'SINGLE'
when e.maritalstatus='M' then 'MARRIED'
end MaritalStatus,
case
when e.Gender='M' then 'Male'
when e.Gender='F' then 'Female'
end Gender,
d.name DepartmentName
from BCMPQA.Employee e join BCMPQA.CONTACT c on e.contactid=c.contactid
join BCMPQA.EmployeeDepartmentHistory ed on e.employeeid=ed.employeeid
join BCMPQA.Department d on d.departmentid=ed.departmentid) SRC 
ON T.ParentEmployeeKey=src.parentemployeekey
where 
src.parentemployeekey is null and (
src.name<>T.Name
or src.hiredate<>T.hiredate
or src.birthdate<>T.birthdate
or src.LoginID<>T.LoginID
or src.emailaddress<>T.emailaddress
or src.Phone<>T.Phoneno
or src.MaritalStatus<>t.MaritalStatus
or src.Gender<>t.Gender
or src.DepartmentName<>t.DepartmentName)
group by t.parentemployeekey


SELECT * FROM Target_DimEmployee where parentemployeekey in (4,6,96,140,274)
select e.EmployeeID parentemployeekey,concat(c.firstname, c.lastname) Name, 
cast(format(e.hiredate, 'dd-MMM-yyyy') as varchar) HireDate, cast(format(e.birthdate, 'dd-MMM-yyyy') as varchar) BirthDate,
concat(substring(c.lastname,1,1), c.firstname) LoginID,
iif(charindex('(',c.phone)!=0,'N/A',phone) Phone,
isnull(c.emailaddress,'N/A') emailaddress,
case
when e.maritalstatus='S' then 'SINGLE'
when e.maritalstatus='M' then 'MARRIED'
end MaritalStatus,
case
when e.Gender='M' then 'Male'
when e.Gender='F' then 'Female'
end Gender,
d.name DepartmentName
from BCMPQA.Employee e join BCMPQA.CONTACT c on e.contactid=c.contactid
join BCMPQA.EmployeeDepartmentHistory ed on e.employeeid=ed.employeeid
join BCMPQA.Department d on d.departmentid=ed.departmentid

---------------------random check--------------------
select e.EmployeeID parentemployeekey,concat(c.firstname, c.lastname) Name, 
cast(format(e.hiredate, 'dd-MMM-yyyy') as varchar) HireDate, cast(format(e.birthdate, 'dd-MMM-yyyy') as varchar) BirthDate,
concat(substring(c.lastname,1,1), c.firstname) LoginID,
iif(charindex('(',c.phone)!=0,'N/A',phone) Phone,
isnull(c.emailaddress,'N/A') emailaddress,
case
when e.maritalstatus='S' then 'SINGLE'
when e.maritalstatus='M' then 'MARRIED'
end MaritalStatus,
case
when e.Gender='M' then 'Male'
when e.Gender='F' then 'Female'
end Gender,
d.name DepartmentName
from BCMPQA.Employee e join BCMPQA.CONTACT c on e.contactid=c.contactid
join BCMPQA.EmployeeDepartmentHistory ed on e.employeeid=ed.employeeid
join BCMPQA.Department d on d.departmentid=ed.departmentid 
where e.EmployeeID=106

SELECT * FROM Target_DimEmployee where ParentEmployeeKey=106

------------------------startficationTest---------------------------
SELECT * FROM Target_DimEmployee;
select * from Traget_Fact_Sales_Quota;

SELECT t.employeekey,dt.timekey,dt.calendaryear,
dt.calendarquarter,s.quantity,s.unit_price,s.salesamount from Target_DimEmployee t join [BCMPQA].[Sales_Quota] s
on s.employeeid=t.parentemployeekey
join [BCMPQA].[DimTime] dt
on s.orderdate=dt.fulldatealternatekey
select * from [BCMPQA].[DimTime]

SELECT sum(s.quantity),sum(s.unit_price),sum(s.salesamount)
from Target_DimEmployee t join [BCMPQA].[Sales_Quota] s
on s.employeeid=t.parentemployeekey
join [BCMPQA].[DimTime] dt
on s.orderdate=dt.fulldatealternatekey

select sum(quantity),sum(unit_price),sum(salesamountquota) from Traget_Fact_Sales_Quota;

------------------------startficationTest--------groupby-------------------
SELECT sum(s.quantity),sum(s.unit_price),sum(s.salesamount)
from Target_DimEmployee t join [BCMPQA].[Sales_Quota] s
on s.employeeid=t.parentemployeekey
join [BCMPQA].[DimTime] dt
on s.orderdate=dt.fulldatealternatekey
group by t.employeekey

select sum(quantity),sum(unit_price),sum(salesamountquota) from Traget_Fact_Sales_Quota
group by employeekey

--------------------rowcount Fact table-------------

CREATE TABLE Traget_Fact_Sales_Quota(
	[EmployeeKey] [int] NULL default -1,
	[TimeKey] [int] NULL default -1,
	[CalendarYear] [int] NULL default 'N/A',
	[CalendarQuarter] [int] NULL default -1,
	[Quantity] [int] NULL,
	[Unit_Price] [money] NULL,
	[SalesAmountQuota] [money] NULL default 0
)

select count(*) from (SELECT t.employeekey,dt.timekey,dt.calendaryear,
dt.calendarquarter,s.quantity,s.unit_price,s.salesamount from Target_DimEmployee t join [BCMPQA].[Sales_Quota] s
on s.employeeid=t.parentemployeekey
join [BCMPQA].[DimTime] dt
on s.orderdate=dt.fulldatealternatekey)a

select count(*) from Traget_Fact_Sales_Quota;

-------------------------reference integreity--FactTable-----------------

SELECT COUNT(*) FROM Traget_Fact_Sales_Quota T
LEFT JOIN (SELECT t.employeekey,dt.timekey,dt.calendaryear,
dt.calendarquarter,s.quantity,s.unit_price,s.salesamount from Target_DimEmployee t join [BCMPQA].[Sales_Quota] s
on s.employeeid=t.parentemployeekey
join [BCMPQA].[DimTime] dt
on s.orderdate=dt.fulldatealternatekey) SRC 
ON T.employeekey=src.employeekey
where src.employeekey is null;

------------------------------------------------------------------------------
select * from Traget_Fact_Sales_Quota

select * from BCMPQA.CONTACT where EmailAddress='null'
select * from BCMPQA.Employee where birthdate>getdate()

select * from [BCMPQA].[DimEmployee] where EmailAddress='null'
select * from BCMPQA.CONTACT where contactid=1121
select * from BCMPQA.Employee where employeeid=124

SELECT * FROM Target_DimEmployee where birthdate>getdate()  ----firstdefect birthdate is greater than tilldate

SELECT * FROM Target_DimEmployee

select * from IN1559.Target_DimEmployee_info  ----informtica target table

CREATE TABLE Traget_Fact_Sales_Quota_info(
	[EmployeeKey] [int] NULL DEFAULT (-1),
	[TimeKey] [int] NULL  DEFAULT (-1),
	[CalendarYear] [int] NULL DEFAULT ('N/A'),
	[CalendarQuarter] [int] NULL DEFAULT (-1),
	[Quantity] [int] NULL,
	[Unit_Price] [money] NULL,
	[SalesAmountQuota] [money] NULL  DEFAULT (0)
) ON [PRIMARY]
GO



select * from IN1559.Target_DimEmployee_info-----info Targettable in dim and also SourceTable in FactTable
select * from [BCMPQA].[DimTime]  -----source table in fact table
select * from [BCMPQA].[Sales_Quota] -----source table in fact table
select * from Traget_Fact_Sales_Quota
select * from IN1559.Traget_Fact_Sales_Quota_info  ----informatica Fact table 